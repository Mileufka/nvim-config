-- List of sub-config files
local core_conf_files = {
    "config.vim",       -- Main vim functionalities configuration file
    --"autocommands.vim", -- List of autocommands
    "mappings.vim",     -- Custom keybindings
    "plugins.lua",      -- Plugins installation
}

vim.loader.enable()

-- Source all sub-config files
for _, file_name in ipairs(core_conf_files) do
    if vim.endswith(file_name, 'vim') then
        vim.cmd(string.format("source %s/core/%s", vim.fn.stdpath("config"), file_name))
    elseif vim.endswith(file_name, 'lua') then
        local module_name, _ = string.gsub(file_name, "%.lua", "")
        package.loaded[module_name] = nil
        require(module_name)
    end
end

