-- Lazy setuplocal
lazypath = vim.fn.stdpath("data") .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
	"git",
	"clone",
	"--filter=blob:none",
	"https://github.com/folke/lazy.nvim.git",
	"--branch=stable",	-- latest stable release
	lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

local prog_langs = { "bash", "c", "cpp", "lua", "python", "sh", "zsh" }

local plugin_specs = {
    {
	-- Colorscheme
	'catppuccin/nvim',
	name = 'catppuccin',
	lazy = false,
	priority = 1000,
	config = function()
	    require('catppuccin').setup({
	    	flavour = "macchiato",
	    })
	    vim.cmd.colorscheme('catppuccin')
	end,
    },
    {
	-- UI improvement
	'stevearc/dressing.nvim',
	event = "BufReadPre",
	config = function()
	    require('dressing').setup()
	end,
    },
    {
	-- Config for the LSP client
	"neovim/nvim-lspconfig",
	event = 'BufReadPost',
	ft = prog_langs,
	config = function()
	    require("config.lsp").setup()
	end,
    },
    {
	-- Linter
	"mfussenegger/nvim-lint",
	event = { "BufWritePost", "BufReadPost", "InsertLeave" },
	linters_by_ft = {
	    bash = { "bash" },
	    c = { "cpplint" },
	    cpp = { "cpplint" },
	    python = { "flake8", "pylint" }
	},
	-- config = function()
	--     require("nvim-lint").setup()
	-- end,
    },
    {
	-- Auto-completion engine
	"hrsh7th/nvim-cmp",
	version = false,
	event = 'InsertEnter',
	dependencies = {
	    "hrsh7th/cmp-nvim-lsp",
	    "hrsh7th/cmp-buffer",
	    "hrsh7th/cmp-path",
	    "onsails/lspkind-nvim",
	    "hrsh7th/cmp-omni",
	    "hrsh7th/cmp-emoji",
	    "L3MON4D3/cmp-luasnip-choice",
	},
	config = function()
	    require("config.nvim-cmp")
	end,
    },
    {
	-- Syntax tree builder
	"nvim-treesitter/nvim-treesitter",
	event = "BufReadPost",
	build = ":TSUpdate",
	--lazy = false,
	dependencies = {
	    "nvim-lua/plenary.nvim",
	},
	config = function()
	    require("config.treesitter")
	end,
    },
    {
	-- Fuzzy-finder over list
	"nvim-telescope/telescope.nvim",
	--optional = true,
	lazy = true,
	cmd = "Telescope",
	dependencies = {
	    "nvim-lua/plenary.nvim",
	    {"nvim-telescope/telescope-fzf-native.nvim", build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'},
	    "nvim-telescope/telescope-symbols.nvim",
	},
	keys = {
	    "<leader>fp", pick, desc = "Projects",
	},
	config = function()
	    require('config.telescope')
	end,
    },
    {
	-- Code runner
	'michaelb/sniprun',
	branch = "master",
	build = "sh install.sh",
	ft = {"c", "cpp", "lua", "python"},
	config = function()
	    require('sniprun').setup()
	end,
    },
    {
	-- File browser
	"nvim-neo-tree/neo-tree.nvim",
	branch = "v3.x",
	dependencies = {
	    "nvim-lua/plenary.nvim",
	    -- not strictly required, but recommended,
	    "nvim-tree/nvim-web-devicons",
	    "MunifTanjim/nui.nvim",
	    -- Optional image support in preview window:
	    -- See `# Preview Mode` for more information
	    "3rd/image.nvim", 
	}
    },
    {
    	-- File navigation tool
	"cbochs/grapple.nvim",
	dependencies = {
	    { "nvim-tree/nvim-web-devicons", lazy = true },
	    "nvim-telescope/telescope.nvim",
	},
	event = { "BufReadPost", "BufNewFile" },
	cmd = "Grapple",
	keys = {
	    { 
	    	"<leader>m",
	    	"<cmd>Grapple toggle<cr>",
	    	desc = "Grapple toggle tag"
	    },
	    {
	    	"<leader>M",
	    	"<cmd>Grapple toggle_tags<cr>",
	    	desc = "Grapple open tags window"
	    },
	    {
	    	"<leader>n",
	    	"<cmd>Grapple cycle_tags next<cr>",
	    	desc = "Grapple cycle next tag"
	    },
	    {
	    	"<leader>p",
	    	"<cmd>Grapple cycle_tags prev<cr>",
	    	desc = "Grapple cycle previous tag"
	    },
	},
	config = function()
	    require('telescope').load_extension('grapple')
	end,   
    },
    {
	-- Snippet engine
	"L3MON4D3/LuaSnip",
	dependencies = 'rafamadriz/friendly-snippets',
	version = "v2.*",
	build = "make install_jsregexp",
	config = function()
	    -- Necessary to load friendly-snippets
	    require("luasnip.loaders.from_vscode").lazy_load()
	end,
    },
    {
	-- Markdown previewer
	"iamcco/markdown-preview.nvim",
	cmd = {
	    "MarkdownPreviewToggle",
	    "MarkdownPreview",
	    "MarkdownPreviewStop"
	},
	ft = "markdown",
	build = function() vim.fn["mkdp#util#install"]() end,
	config = function()
	    require("config.markdown-preview")
	end,
    },
    {
	-- Diagnostic tool
	'folke/trouble.nvim',
	dependencies = "nvim-tree/nvim-web-devicons",
	cmd = "Trouble",
	ft = prog_langs,
	config = function()
	    require("config.trouble")
	end,
    },
    {
	-- Status line
	"nvim-lualine/lualine.nvim",
	event = "VeryLazy",
	config = function()
	    require("config.lualine")
	end,
    },
    {
	-- Tab line
	'akinsho/bufferline.nvim',
	version = "*",
	dependencies = 'nvim-tree/nvim-web-devicons',
	event = { "BufEnter" },
	--cond = firenvim_not_active,
	config = function()
	    require("config.bufferline")
	end,
    },
    {
	-- Startup screen
	'nvimdev/dashboard-nvim',
	dependencies = 'nvim-tree/nvim-web-devicons',
	event = 'VimEnter',
	config = function()
	    require('config.dashboard')
	end,
    },
 --    {
 --    	-- Startup banchmark tool
	-- "dstein64/vim-startuptime",
	-- cmd = "StartupTime",
	-- config = function()
	--     vim.g.startuptime_tries = 10
	-- end,
 --    },
    {
	-- Git
	'lewis6991/gitsigns.nvim',
	event = "BufReadPost",
	config = function()
	    require("config.gitsigns").setup()
	end,
    },
    {
	-- Indentation guide
	'lukas-reineke/indent-blankline.nvim',
	event = "BufReadPost",
	main = "ibl",
	config = function()
	    require("config.indent-blankline")
	end,
    },
    {
	-- Comments
	'numToStr/Comment.nvim',
	lazy = false,
	config = function()
	    require('Comment').setup()
	end,
    },
    {
	-- Notification manager
	'rcarriga/nvim-notify',
	event = "VeryLazy",
	config = function()
	    require("config.nvim-notify")
	end,
    },
    {
	-- Debug Adapter Protocol
	'mfussenegger/nvim-dap',
	dependencies = 'rcarriga/nvim-dap-ui',
	keys = {
	    { "<leader>d", "", desc = "+debug", mode = {"n", "v"} },
	    { "<leader>dB", function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, desc = "Breakpoint Condition" },
	    { "<leader>db", function() require("dap").toggle_breakpoint() end, desc = "Toggle Breakpoint" },
	    { "<leader>dc", function() require("dap").continue() end, desc = "Continue" },
	    { "<leader>da", function() require("dap").continue({ before = get_args }) end, desc = "Run with Args" },
	    { "<leader>dC", function() require("dap").run_to_cursor() end, desc = "Run to Cursor" },
	    { "<leader>dg", function() require("dap").goto_() end, desc = "Go to Line (No Execute)" },
	    { "<leader>di", function() require("dap").step_into() end, desc = "Step Into" },
	    { "<leader>dj", function() require("dap").down() end, desc = "Down" },
	    { "<leader>dk", function() require("dap").up() end, desc = "Up" },
	    { "<leader>dl", function() require("dap").run_last() end, desc = "Run Last" },
	    { "<leader>do", function() require("dap").step_out() end, desc = "Step Out" },
	    { "<leader>dO", function() require("dap").step_over() end, desc = "Step Over" },
	    { "<leader>dp", function() require("dap").pause() end, desc = "Pause" },
	    { "<leader>dr", function() require("dap").repl.toggle() end, desc = "Toggle REPL" },
	    { "<leader>ds", function() require("dap").session() end, desc = "Session" },
	    { "<leader>dt", function() require("dap").terminate() end, desc = "Terminate" },
	    { "<leader>dw", function() require("dap.ui.widgets").hover() end, desc = "Widgets" },
	},
	config = function()
	    require('config.nvim-dap')
	end,
    },
    {
	-- Debug Adapter Protocol UI
	'rcarriga/nvim-dap-ui',
	dependencies = 'nvim-neotest/nvim-nio',
	config = function()
	    require('dapui').setup()
	end,
    },
    {
	-- Project manager
	"ahmedkhalf/project.nvim",
	event = "VeryLazy",
	config = function()
	    require('project_nvim').setup()
	    --require('telescope').load_extension('projects')
	end,
    },
    {
	'altermo/ultimate-autopair.nvim',
	event={'InsertEnter','CmdlineEnter'},
	--recommended as each new version will have breaking changes
	branch='v0.6',
	config = function()
	    require('ultimate-autopair').setup()
	end
    },
    {
	-- Surround
	"kylechui/nvim-surround",
	-- Use for stability; omit to use `main` branch for the latest features
	version = "*",
	event = "VeryLazy",
	config = function()
	    require("nvim-surround").setup()
	end
    },
    {
    	-- Cursor line number mode indicator
	'mawkler/modicator.nvim',
	dependencies = 'catppuccin/nvim', -- Add your colorscheme plugin here
	config = function()
	    require('modicator').setup()
	end
    },
    {
	"m4xshen/hardtime.nvim",
	dependencies = {
	    "MunifTanjim/nui.nvim",
	    "nvim-lua/plenary.nvim",
	},
	config = function()
	    require('config.hardtime')
	end,
    },
}

-- Configuration for Lazy itself
local lazy_opts = {
    ui = {
	border = "rounded",
	title = "Plugin Manager",
	title_pos = "center",
	icons = {
	    ft = "",
	    lazy = "󰂠",
	    loaded = "",
	    not_loaded = "",
	},
    },
    performance = {
    	rtp = {
    	    disabled_plugins = {
		"2html_plugin",
		"netrwPlugin",
		"gzip",
		"tarPlugin",
		"tohtml",
		"tutor",
		"zipPlugin",
	    },
	},
    },
}require("lazy").setup(plugin_specs, lazy_opts)
