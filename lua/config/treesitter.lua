require("nvim-treesitter.configs").setup {
    ensure_installed = { "c", "cpp", "cmake", "diff", "lua", "make",
			 "markdown", "vim", "vimdoc" },
    ignore_install = {}, -- List of parsers to ignore installing
    sync_install = true,
    auto_install = true,
    highlight = {
	enable = true, -- false will disable the whole extension
	disable = { 'help' }, -- list of language that will be disabled
    },
    indent = {
	enable = true,
    }
}
