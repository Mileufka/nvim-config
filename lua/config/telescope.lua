local actions = require('telescope.actions')
local open_with_trouble = require("trouble.sources.telescope").open
local add_to_trouble = require("trouble.sources.telescope").add

require('telescope').setup {
    defaults = {
	mappings = {
	    i = {
		["<C-n>"] = actions.cycle_history_next,
		["<C-p>"] = actions.cycle_history_prev,

		["<C-j>"] = actions.move_selection_next,
		["<C-k>"] = actions.move_selection_previous,

		["<C-q>"] = actions.close,

		["<C-t>"] = open_with_trouble,
	    },
	    n = {
		["<esc>"] = actions.close,
		["j"] = actions.move_selection_next,
		["k"] = actions.move_selection_previous,
		["q"] = actions.close,

		["<C-t>"] = open_with_trouble,
	    },
	},
    }
}

require('telescope').load_extension('fzf')
