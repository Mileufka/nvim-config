-- Set up nvim-cmp.

local cmp = require('cmp')
local lspkind = require('lspkind')
local luasnip = require('luasnip')

local kind_icons = {
    Text = "",
    Method = "󰆧",
    Function = "󰊕",
    Constructor = "",
    Field = "󰇽",
    Variable = "󰂡",
    Class = "󰠱",
    Interface = "",
    Module = "",
    Property = "󰜢",
    Unit = "",
    Value = "󰎠",
    Enum = "",
    Keyword = "󰌋",
    Snippet = "",
    Color = "󰏘",
    File = "󰈙",
    Reference = "",
    Folder = "󰉋",
    EnumMember = "",
    Constant = "󰏿",
    Struct = "",
    Event = "",
    Operator = "󰆕",
    TypeParameter = "󰅲",
}

cmp.setup {
    completion = {
	keyword_length = 3,
	completeopt = "menu, noselect",
    },
    formatting = {
	format = function(entry, vim_item)
	    -- Kind icons - This concatenates the icons with the name of the item kind
	    vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
	    -- Source
	    vim_item.menu = ({
		buffer = "[Buffer]",
		nvim_lsp = "[LSP]",
		luasnip = "[LuaSnip]",
		nvim_lua = "[Lua]",
		-- latex_symbols = "[LaTeX]",
	    })[entry.source.name]
	    return vim_item
	end
    },
    mapping = cmp.mapping.preset.insert {
	['<CR>'] = cmp.mapping(function(fallback)
	    if cmp.visible() then
		if luasnip.expandable() then
		    luasnip.expand()
		else
		    cmp.confirm({
			select = true,
		    })
		end
	    else
		fallback()
	    end
	end),
	["<Tab>"] = cmp.mapping(function(fallback)
	    if cmp.visible() then
		cmp.select_next_item()
	    elseif luasnip.locally_jumpable(1) then
		luasnip.jump(1)
	    else
		fallback()
	    end
	end, { "i", "s" }),
	["<S-Tab>"] = cmp.mapping(function(fallback)
	    if cmp.visible() then
		cmp.select_prev_item()
	    elseif luasnip.locally_jumpable(-1) then
		luasnip.jump(-1)
	    else
	    	fallback()
	    end
	end, { "i", "s" }),
	['<C-Space>'] = cmp.mapping.complete(),
	-- ["<S-CR>"] = LazyVim.cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace }),
	["<C-CR>"] = function(fallback)
	    cmp.abort()
	    fallback()
        end,
	["<C-b>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
    },
    snippet = {
	expand = function(args)
	    luasnip.lsp_expand(args.body)
	end,
    },
    sources = {
	{ name = 'nvim_lsp' },
	{ name = 'buffer', keyword_length = 3 },
	{ name = 'path' },
	{ name = 'luasnip' },
	{ name = 'emoji', insert = true },
    },
    enabled = function()
    	-- disable completion in comments
    	local context = require 'cmp.config.context'
    	-- keep command mode completion enabled when cursor is in a comment
    	if vim.api.nvim_get_mode().mode == 'c' then
    	    return true
	else
	    return not context.in_treesitter_capture("comment")
	    	and not context.in_syntax_group("Comment")
	end
    end,
}

cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
    	{ name = 'buffer' }
    }
})

cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
    	{ name = 'path', }
    }, {
    	{ name = 'cmdline', }
    }),
    matching = { 
    	disallow_symbol_nonprefix_matching = false,
    },
})
