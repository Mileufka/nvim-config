local M ={}

local map = vim.keymap.set;

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
M.on_attach = function(client, bufnr)

    local function opts(desc)
	return {buffer = bufnr, desc = "LSP " .. desc}
    end
    map('n', 'gD', vim.lsp.buf.declaration, opts "Go to declaration")
    map('n', 'gd', vim.lsp.buf.definition, opts "Go to definition")
    map('n', 'K', vim.lsp.buf.hover, opts " ")
    map('n', 'gi', vim.lsp.buf.implementation, opts "Go to implementation")
    map('n', '<C-k>', vim.lsp.buf.signature_help, opts "Show signature help")
    map('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, opts "Add workspace folder")
    map('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, opts "Remove workspace folder")
    map('n', '<leader>wl', function()
	print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, opts "List workspace folder")
    map('n', '<leader>D', vim.lsp.buf.type_definition, opts "Go to type definition")
    map('n', '<leader>rn', vim.lsp.buf.rename, opts "Rename")
    map({ 'n', 'v' }, '<leader>ca', vim.lsp.buf.code_action, opts "Code action")
    map('n', 'gr', vim.lsp.buf.references, opts "Show references")
    map('n', '<leader>f', function()
	vim.lsp.buf.format { async = true }
    end, opts "Format")
end

-- disable semanticTokens
M.on_init = function(client, _)
    if client.supports_method "textDocument/semanticTokens" then
	client.server_capabilities.semanticTokensProvider = nil
    end
end

M.capabilities = require('cmp_nvim_lsp').default_capabilities()

M.capabilities.textDocument.completion.completionItem = {
    documentationFormat = { "markdown", "plaintext" },
    snippetSupport = true,
    preselectSupport = true,
    insertReplaceSupport = true,
    labelDetailsSupport = true,
    deprecatedSupport = true,
    commitCharactersSupport = true,
    tagSupport = { valueSet = { 1 } },
    resolveSupport = {
	properties = {
	    "documentation",
	    "detail",
	    "additionalTextEdits",
	},
    },
}

M.setup = function()

    local lspconfig = require('lspconfig')

    map('n', '<leader>e', vim.diagnostic.open_float)
    map('n', '[d', vim.diagnostic.goto_prev)
    map('n', ']d', vim.diagnostic.goto_next)
    map('n', '<leader>q', vim.diagnostic.setloclist)

    vim.diagnostic.config {
	underline = false,
	virtual_text = true,
	signs = true,
	severity_sort = true,
    }

    vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
	border = "rounded",
    })

 --    lspconfig.setup {
	-- on_attach = M.on_attach,
	-- capabilities = M.capabilities,
	-- on_init = M.on_init,
 --    }

    lspconfig.clangd.setup {
	--on_attach = custom_attach,
	-- capabilities = capabilities,
	filetypes = { "c", "cpp", "objc", "objcpp", "cuda", "proto" },
	flags = {
	    debounce_text_changes = 500,
	},
    }
end

return M
-- Change diagnostic signs.
-- sign("DiagnosticSignError", { text = '🆇', texthl = "DiagnosticSignError" })
-- sign("DiagnosticSignWarn", { text = '⚠️', texthl = "DiagnosticSignWarn" })
-- sign("DiagnosticSignInfo", { text = 'ℹ️', texthl = "DiagnosticSignInfo" })
-- sign("DiagnosticSignHint", { text = '', texthl = "DiagnosticSignHint" })

-- Global config for diagnostic

-- Change border of documentation hover window, See https://github.com/neovim/neovim/pull/13998.

