local symbols = require("trouble").statusline({
    mode = "lsp_document_symbols",
    groups = {},
    title = false,
    filter = { range = true },
    format = "{kind_icon}{symbol.name:Normal}",
    -- The following line is needed to fix the background color
    -- Set it to the lualine section you want to use
    hl_group = "lualine_c_normal",
})

require('lualine').setup {
    options = {
	theme = 'catppuccin',
    },
    sections = {
	-- lualine_a = {'mode'},
	-- lualine_b = {'branch', 'diff', 'diagnostics'},
	lualine_c = {'filename', {symbols.get, cond = symbols.has,}},
	-- lualine_x = {'encoding', 'fileformat', 'filetype'},
	-- lualine_y = {'progress'},
	-- lualine_z = {'location'}
    },
    extensions = {'trouble'}
}
