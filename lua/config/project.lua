require('project_nvim').setup {
    manual_mode = true,
    LazyVim.on_load("telescope.nvim", function()
	require('telescope').load_extension('project')
    end)
}
