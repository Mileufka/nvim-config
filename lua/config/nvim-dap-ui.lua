local dap, dapui = require('dap'), require('dapui')

dapui.setup {
    keys = {
	{ "<leader>du", function() toggle({ }) end, desc = "Dap UI" },
	{ "<leader>de", function() eval() end, desc = "Eval", mode = {"n", "v"} },
    },
}

dap.listener.before = {
    attach.dapui_config = function() dapui.open() end,
    launch.dapui_config = function() dapui.open() end,
    event_terminated.dapui_config = function() dapui.close() end,
    event_exited.dapui_config = function() dapui.close() end,
}

