" Remap escape to "jk"
inoremap jk <ESC>

" Search will center on the line it's found in
nnoremap n nzzzv
nnoremap N Nzzzv

" Double tap escape key to clear search highlighting
nnoremap <esc><esc> :silent! nohlsearch<cr>

" Quick buffer navigation for azerty layout
noremap <A-&> :buffer 1<cr>
noremap <A-é> :buffer 2<cr>
noremap <A-"> :buffer 3<cr>
noremap <A-'> :buffer 4<cr>
noremap <A-(> :buffer 5<cr>
noremap <A--> :buffer 6<cr>
noremap <A-è> :buffer 7<cr>
noremap <A-_> :buffer 8<cr>
noremap <A-ç> :buffer 9<cr>
noremap <a-h> :bfirst<cr>
noremap <a-j> :bprev<cr>
noremap <a-k> :bnext<cr>
noremap <a-l> :blast<cr>

" Replace word under the cursor
nnoremap <leader>j *``cgn

" Replace selection
xnoremap <leader>j y<cmd>let @/=escape(@", '/')<cr>"_cgn
"xnoremap <leader>j y<cmd>substitute(escape(@", '/'), '\n', '\\n', 'g')<cr>"_cgn

" Rename variables
"lua require('lspconfig').tsserver.setup({})
"nnoremap <F2> <cmd>lua vim.lsp.buf.rename()<cr>
