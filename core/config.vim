" Clipboard setting, always use clipboard for all delete, yank, change, and
" put
if !empty(provider#clipboard#Executable())
	if has("unix")
    	set clipboard+=unnamed
	elseif has("win32")
    	set clipboard+=unnamedplus
	endif
endif

let mapleader=","

" Do not save backup files
set nobackup

" Spelling
" set spell spelllang=en_us,fr

" Encoding
set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

" Disable regex-based syntax highlighting in favor of Tree-sitter
syntax off

" COMMANDS --------------------------------------------------------------- {{{

" File extensions wildmenu will ignore
set wildignore+=*.o,*.obj,*.bin,*.dll,*.exe
set wildignore+=*.jpg,*.jpeg,*.png,*.bmp,*.gif,*.tiff,*.svg,*.ico

set wildignorecase

set wildmode=list:longest

" }}}

" USER INTERFACE --------------------------------------------------------- {{{

" Show line numbers relative to current line
set number
set relativenumber

" Highlight cursor line and column underneath the cursor
set cursorline
" set cursorcolumn

" Set number of lines for commands
set cmdheight=2

" Graphic mode specific options
if has("gui_running")

	" Set font
	if has("linux")
		set guifont=Fira\ Code\ 10
		set guiligatures=!\"#$%&()*+-./:<=>?@[]^_{\|~
	elseif has("win32")
		set guifont=FiraCode\ Nerd\ Font\ Mono:h11
	endif
    
    " Remove toolbar
    set guioptions-=T

    " Remove menu bar
    set guioptions-=m

    " Remove right-hand scrollbar
    set guioptions-=r
    set guioptions-=R

    " Remove left-hand scrollbar
    set guioptions-=l
    set guioptions-=L

    " Tab label is only open file name
    set guitablabel=%t

endif

" }}}

" TEXT FORMATING --------------------------------------------------------- {{{

" Tab width
"set tabstop=4

set shiftwidth=4    " Number of spaces to use for each step of autoindent

" Copy indent formating of previous line
set copyindent

" }}}

" SEARCH ----------------------------------------------------------------- {{{

" Ignore case when searching
set ignorecase

" Automatically switch search to case-sensitive when search query contains 
" any uppercase letter
set smartcase

" }}}
