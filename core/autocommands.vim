if has ('autocmd')
	" Turn off search highlighting when search is done
	"augroup vimrc-incsearch-highlight
	"    autocmd!
	"    autocmd CmdlineEnter /,\? :set hlsearch
	"    autocmd CmdlineLeave /,\? :set nohlsearch
	"augroup END
	
	"augroup vimrc-sync-fromstart
	"	autocmd!
	"	autocmd BufEnter * :syntax sync maxlines=200
	"augroup END
endif
