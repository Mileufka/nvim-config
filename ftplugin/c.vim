" TEXT FORMATING --------------------------------------------------------- {{{
" Every wrapped line will continue visually indented
set breakindent

set expandtab       " Tabulations are made of space
set shiftwidth=4    " Number of spaces to use for each step of autoindent
set softtabstop=4   " Number of spaces per tabulation
set shiftround      " Round indent to multiple of shiftwidth

" Enable automatic C program indenting
set cindent

" Add 1 shiftwidth wide indent after automatic indent
"set cinoptions="1s"

" Auto-indent when inserting a case
set cinwords+=case

" }}}
